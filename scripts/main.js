function createQuestion() {
  // Hilfsfunktion: Gibt zufällig die Indizes von vier Ländern
  // in Array mit Länderinfo zurück
  function getRandomIndices() {
    let indices = [];
    while (indices.length !== 4) {
      let index = parseInt(Math.random() * countryInfos.length);
      if (indices.indexOf(index) === -1) {
        indices.push(index);
      }
    }
    return indices;
  }

  // Hilfsfunktion: Gibt Callback Funktion für Event-Listener
  // zurück, die Element, auf das geklickt wird definierte
  // Klasse hinzufügt
  function addClass(className) {
    return (e) => {
      const elFlag = e.currentTarget;
      elFlag.classList.add(className);
    };
  }

  // Hilfsfunktion: Erstellte Element mit Bild von
  // Flagge, auf das geklickt werden kann
  function createCountryEl(index, isCorrect) {
    const country = countryInfos[index];
    const elFlag = document.createElement('div');
    elFlag.setAttribute('class', 'flag');
    elFlag.addEventListener(
      'click',
      addClass(isCorrect ? 'correct' : 'incorrect')
    );
    const elImg = document.createElement('img');
    elImg.setAttribute('src', country.flag.src);
    elImg.setAttribute('alt', country.flag.alt);
    elFlag.appendChild(elImg);
    return elFlag;
  }

  // Zufällig vier Länder auswählen
  const indices = getRandomIndices();
  // Gesuchtes Land auswählen
  const correctIdx = indices[0];
  const correct = countryInfos[correctIdx];
  // String mit allen ausgewählten Ländern erstellen
  let countriesStr = indices.reduce(
    (strSoFar, i, idx) =>
      strSoFar +
      countryInfos[i].translation +
      (idx !== indices.length - 2 ? ', ' : ' und '),
    ''
  );
  countriesStr = countriesStr.substring(0, countriesStr.length - 2);
  // Frage entsprechendem Element hinzufügen
  const elQuestion = document.getElementById('question');
  elQuestion.textContent =
    `Finde unter den Flaggen von ${countriesStr} ` +
    `die Flagge von ${correct.translation}?`;
  // Sortieren, damit richtige Flagge nicht immer an 1. Stelle
  indices.sort((val1, val2) => {
    if (val1 < val2) {
      return -1;
    }
    return 1;
  });
  // Flaggen erster Spalte hinzufügen
  el1stCol = document.querySelector('div.col.first');
  el1stCol.innerHTML = '';
  for (let i = 0; i < 2; i++) {
    const countrIdx = indices[i];
    const elFlag = createCountryEl(countrIdx, countrIdx === correctIdx);
    el1stCol.appendChild(elFlag);
  }
  // Flaggen erster Spalte hinzufügen
  el2ndCol = document.querySelector('div.col.second');
  el2ndCol.innerHTML = '';
  for (let i = 2; i < 4; i++) {
    const countrIdx = indices[i];
    const elFlag = createCountryEl(countrIdx, countrIdx === correctIdx);
    el2ndCol.appendChild(elFlag);
  }
}

function loadCountries(regionToFilterr) {
  return axios.get('https://restcountries.com/v3.1/all').then((response) => {
    const countries = response.data;
    for (let i = 0; i < countries.length; i++) {
      const country = countries[i];
      if (country.region === regionToFilterr) {
        // Für Fragen notwendige Informationen der Länder
        // in Objekt speichern und globalem Array hinzufügen
        countryInfos.push({
          name: country.name.common,
          translation: country.translations.deu.common,
          flag: {
            src: country.flags.svg,
            alt: country.flags.alt,
          },
        });
      }
    }
  });
}

// Zu Beginn alle Länderdaten von europäischen Ländern
// laden und erste Frage estellen
const countryInfos = [];
loadCountries('Europe').then(createQuestion);

// Bei Klick auf 'Neue Frage' Button neue Frage erstellen
const elBtnNew = document.getElementById('new-question');
elBtnNew.addEventListener('click', createQuestion);
